﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;

public class Program
{
    public static void Main()
    {
        var resource = Assembly.GetExecutingAssembly().GetManifestResourceStream("ImageDotsToData.dots.png");

        byte[] buffer = new byte[resource.Length];
        resource.Read(buffer, 0, buffer.Length);

        var positions = GetDotsPositions(buffer);

        foreach (var position in positions)
        {
            Console.WriteLine(position);
        }

        Console.ReadLine();
    }

    public static List<Tuple<int, int>> GetDotsPositions(byte[] imgData)
    {
        int HEIGHT = 400;
        int WIDTH = 600;

        List<Tuple<int, int>> positions = new List<Tuple<int, int>>();

        // get the jpg image
        Bitmap bitmap;
        using (Stream bmpStream = new MemoryStream(imgData))
        {
            Image image = Image.FromStream(bmpStream);
            bitmap = new Bitmap(image);
        }

        List<int> knownBlankColors = new List<int>();
        knownBlankColors.Add(Color.White.ToArgb());

        for (int x = 0; x < HEIGHT; x++)
        {
            for (int y = 0; y < WIDTH; y++)
            {
                Color pixelColor = bitmap.GetPixel(x, y);

                var code = pixelColor.ToArgb();

                if (!knownBlankColors.Contains(code))
                    positions.Add(new Tuple<int, int>(x, y));
                // check if it's black or a shade of black
                // e.g. if it belongs to an array of colors..etc
                // if so, record the coordinates (x,y)
            }
        }

        return positions;
    }
}